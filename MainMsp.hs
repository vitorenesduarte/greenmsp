{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE BangPatterns #-}

module Main where

import Foreign
import Foreign.C.Types
import Msp
import ReadsToJSON
import System.Environment
import Control.Concurrent

foreign import ccall unsafe "rapl/lib.h raplInit"
  raplInitC :: Int -> Int

foreign import ccall unsafe "rapl/lib.h raplMeasure"
  raplMeasureC :: Int -> Double

raplInit :: Int -> Int
raplInit = raplInitC

raplMeasure :: Int -> Double
raplMeasure = raplMeasureC

type Reads = [(String, String, Double)] -- [(methodParentName, methodName, joules)]

main :: IO ()
main =  
  do args <- getArgs
     strMsp <- readFile (head args)
     let progMsp = (read strMsp) :: Msp 
     let readsJSONFile = head(tail(args))
     let !success = raplInit 0

     readsMVar <- newEmptyMVar
     methodsMVar <- newEmptyMVar
     doneMspMVar <- newEmptyMVar

     let !methods = ["main", ""] -- parent of main is "". Is there a better way to do this?
     !p <- putMVar methodsMVar methods

     forkOS $ runMsp progMsp methodsMVar doneMspMVar

     raplLoop [] readsMVar methodsMVar doneMspMVar
     reads <- takeMVar readsMVar
     --putStrLn $ show reads
     readsToJSONFile readsJSONFile reads

raplLoop :: Reads -> MVar Reads -> MVar Methods -> MVar Bool -> IO ()
raplLoop reads readsMVar methodsMVar doneMspMVar = do isEmpty <- isEmptyMVar doneMspMVar
                                                      case isEmpty of 
                                                         False -> putMVar readsMVar (reverse reads)
                                                         True -> waitAndReddit reads readsMVar methodsMVar doneMspMVar

waitAndReddit :: Reads -> MVar Reads -> MVar Methods -> MVar Bool -> IO ()
waitAndReddit reads readsMVar methodsMVar doneMspMVar = do let !read = raplMeasureC 0
                                                           !methods <- takeMVar methodsMVar
                                                           let !method = head(methods)
                                                           let !parent = head(tail(methods))
                                                           !p <- putMVar methodsMVar methods
                                                           threadDelay 100000
                                                           raplLoop ((parent, method, read):reads) readsMVar methodsMVar doneMspMVar

runMsp :: Msp -> MVar Methods -> MVar Bool -> IO ()
runMsp progMsp methodsMVar doneMspMVar = do runMspProg progMsp methodsMVar
                                            putMVar doneMspMVar True
