msp : rapl/lib.c rapl/rapl.c Msp.lhs MainMsp.hs
	gcc -O2 -Wall -c rapl/rapl.c
	gcc -O2 -Wall -c rapl/lib.c
	ghc --make MainMsp.hs lib.o rapl.o -o msp -lm -threaded
	chmod u+x msp

doc: SL.lhs Msp.lhs
	pdflatex Msp.lhs
	pdflatex SL.lhs

clean:
	rm -f *.aux *.hi *.log *.o 

cleanall:
	rm -f *.aux *.hi *.log *.o ./msp *.pdf
