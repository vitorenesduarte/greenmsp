# GreenMSP
## Análise e Transformação de Software (2015/2016)

---

#### Dependências:

- __Aeson__: A fast Haskell JSON library

```bash
$ cabal install aeson
```


#### Ficheiros:

- __Msp.lhs__: máquina virtual 
- __MainMsp.hs__: função main que corre numa thread o programa MSP que é passado como argumento. Numa segunda thread, a cada 100000 microseconds, faz uma leitura do consumo. A cada leitura está associado o nome do método que está a ser executado no momento em que a leitura ocorreu.
- __SL.lhs__: linguagem SL e tradução para MSP
- __examples/maiorDeDoisNums.msp__: programa msp que lê dois número e indica qual o maior. (este programa foi gerado a partir de um programa SL, ver em baixo)
- __examples/tc1_maiorDedois__: caso de teste para o programa anterior (ver em baixo como utilizar)
- __examples/*__: outros exemplos
- __rapl/rapl.c__: libraria que simplifica o acesso aos valores RAPL presentes nos MSR registers dos CPU Intel modernos
- __rapl/lib.c__: wrapper sobre alguns métodos da libraria __rapl/rapl.c__. Disponibiliza dois métodos: __raplInit(int core)__ e __raplMeasure(int core)__
- __ReadsToJSON.hs__: biblioteca que disponibiliza o método __readsToJSONFile :: FilePath -> [(String, Double)] -> IO ()__ que escreve um ficheiro __.json__ com as leituras. 

```json
{
  "reads": [
    {
      "value": 13835.418014526,
      "method": "main"
    },
    {
      "value": 13835.75793457,
      "method": "main"
    },
    // ...
  ]
}
```

- __charts/charts.html__: Lê o ficheiro __.json__ gerado pela biblioteca anterior, e desenha o respetivo gráfico usando [HighchartsJS](http://www.highcharts.com/). Também é possível fazer download do gráfico em vários formatos (PNG, JPEG, PDF, SVG). Para correr:

```bash
$ bash show-me-a-chart.sh
```
---

#### Compilar a Máquina Virtual:

Para criar um programa executável para MSP deve instalar o compilador de __Haskell__ ghc (disponível em [www.haskell.org](www.haskell.org)) e depois:

```bash
make msp
```
---

#### Executar a Máquina Virtual

Para executar a máquina virtual basta dar um programa MSP como argumento, por exemplo o programa __examples/maiorDeDoisNums.msp__, e o nome do ficheiro para onde queremos que as leituras sejam escritas. Como também queremos calcular o consumo usando o RAPL temos que antes correr `sudo modprobe msr`: 
```
$ sudo modprobe msr
$ sudo ./msp examples/maiorDeDoisNums.msp charts/reads.json
Found Ivybridge CPU
Introduza um inteiro:
4
Introduza um inteiro:
9
s:1
s:3
9
```

---

#### Execução de Casos de Teste

A execução dos programas pode ser feita automaticamente a partir de casos de teste. O ficheiro __examples/tc1_maiorDedois__ contém um caso de teste, que pode ser passado ao programa anterior para executar o programa:

```bash
$ sudo modprobe msr
$ sudo ./msp examples/maiorDeDoisNums.msp charts/reads.json < examples/tc1_maiorDedois 
Found Ivybridge CPU
Introduza um inteiro:
Introduza um inteiro:
s:1
s:3
7
```
---

Obviamente, que o caso de teste não está completo, pois deverá conter também o resultado esperado. Após a execução do programa o resultado produzido deve ser comparado com o esperado, para se determinar se o caso de teste falhou ou não.

---

#### O Interpretador da Linguagem "Simple Language" (SL)

Para melhor testar a máquina virtual pode gerar código MSP usando o tradutor da linguagem __SL__.  Para o fazer, deve usar o interpretador de __Haskell__ ghci (instaldo com o ghc):

```bash
$ ghci SL.lhs
```

Dentro do interpretador ghci, pode escrever a seguinte expressão __Haskell__

```
*SL> compileSL2Msp progSL2 "prog.msp"
```

que compila o programa progSL2, definido em SL.lhs, para MSP, gerando o ficheiro __prog.msp__.

Para executar este programa basta usar de novo o interpretador de __msp__.

---

#### Monitorização de Programas SL

Para a monitorização dos statements que são usados durante a execução de um programa, foi definido uma função que faz a instrumentação dos programas. Por exemplo, para instrumentar o programa que lê dois números e determina o maior, basta fazer no ghci:

```bash
*SL> compileSL_sfl maiorDeDoisNums "maiorDeDoisNums.msp" 
```

O compliador de SL traduz o programa SL maiorDeDoisNums (definido no ficheiro __SL.lhs__) para MSP e grava esse código no ficheiro maiorDeDoisNums.msp (i.e., o ficheiro incluído nesta distribuição).

---

#### Documentação

Para produzir a documentação da máquina virtual e da linguagem SL basta:

```bash
$ make doc
```

---

#### Limpeza

Para apagar os ficheiros temporários:

```bash
$ make clean
```

Para apagar o pdf e o executável:

```bash
$ make cleanall
```
